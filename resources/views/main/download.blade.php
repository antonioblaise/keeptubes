@extends('layouts.master')
@section('title', $data['data']->title.' -- KeepTubes.com')
@section('content')
	<h3>{{ $data['data']->title }}</h3>
	<p>{{ Normalizer::normalize($data['data']->title) }}</p>
    <video src="<?php echo $data['data']->url;?>" controls></video>
@endsection